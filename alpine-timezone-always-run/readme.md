# Docker File Creating Alpine Timezone Set

```Dockerfile
FROM alpine:latest
RUN apk add --no-cache tzdata
ENV TZ=Europe/Istanbul
```

Output:
```shell
/ # date
Wed Dec 14 21:42:22 +03 2022
```

# Always Alpine Container Run

```
docker run -d --name test alpine-timezone tail -f /dev/null
```
